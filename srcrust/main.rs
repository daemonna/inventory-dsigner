extern crate clap;
pub mod inventory;
use clap::{Arg, App};

fn main() {

    //PARAMS
    let matches = App::new("Inventory dSigner")
        .version("0.1.0")
        .author("peter ducai <peter.ducai@gmail.com>")
        .about("tool for Ansible like inventories")
        .arg(Arg::with_name("inf")
                 .short("i")
                 .long("in")
                 .takes_value(true)
                 .help("input file"))
        .arg(Arg::with_name("outf")
                 .short("o")
                 .long("out")
                 .takes_value(true)
                 .help("output file"))
        .arg(Arg::with_name("fd")
                 .short("fd")
                 .long("fromdir")
                 .takes_value(true)
                 .help("create single inventory file from directory"))
        .arg(Arg::with_name("td")
                 .short("td")
                 .long("filetodir")
                 .takes_value(true)
                 .help("create directory tree from single file"))
        .get_matches();

    //PROCESS PARAMS
    if let Some(i) = matches.value_of("inf") {
        println!("input file: {}", i);
    }

    if let Some(o) = matches.value_of("outf") {
        println!("output file: {}", o);
    }
    
    if let Some(f) = matches.value_of("fd") {
        println!("fd: {}", f);
    }

    // if i == "" {
    //     println!("Cannot use single file and directory as input file");
    // }

    if let Some(t) = matches.value_of("td") {
        println!("td: {}", t);
    }

    //CREATE INVENTORY
    let mut p1 = inventory::Inventory::default();
    p1.name = String::from("example_inventory");
    p1.current_group = String::from("ungrouped");
    println!("{:?}", p1);

}



// mod inventory;

// use std::env;
// use crate::inventory::inventory::Inventory;

// fn main() {
//     println!("Inventory dSigner");
    
//     let mut invmain = Inventory{
//         name: String::from("example inventory"),
//         hosts: String::from("hosts"),
//         groups: String::from("groups"),
//         vars: String::from("vars")
//     };

//     //test
//     invmain.name = String::from("in1");
//     println!("{:?}", invmain);

//     let args: Vec<String> = env::args().collect();

//     let query = &args[1];
//     let filename = &args[2];

//     println!("Searching for {}", query);
//     println!("In file {}", filename);
// }
