use std::fs;
use std::fs::File;
use std::io::{Write, BufReader, BufRead, Error};

pub fn write_file() -> Result<(), Error> {
    let path = "lines.txt";

    let mut output = File::create(path)?;
    write!(output, "Rust\n💖\nFun")?;

    let input = File::open(path)?;
    let buffered = BufReader::new(input);

    for line in buffered.lines() {
        println!("{}", line?);
    }

    Ok(())
}

pub fn createdir() -> std::io::Result<()> {
    fs::create_dir_all("/some/dir")?;  //Recursively create a directory and all of its parent components if they are missing.
    Ok(())
}