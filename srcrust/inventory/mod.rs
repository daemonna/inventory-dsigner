pub use self::inventory::Inventory;
pub mod inventory {

    #[derive(Debug)]
    pub struct Host {
        pub name: String,
        pub vars: Vec<String>,
    }
    
    #[derive(Debug)]
    pub struct Group {
        pub name: String,
        pub vars: Vec<String>,
        pub hosts: Vec<Host>,
    }

    #[derive(Debug)]
    pub struct Inventory {
        pub name: String,
        pub hosts: Vec<Host>,
        pub groups: Vec<Group>,
        pub vars: Vec<String>,
        pub current_group: String,
        pub ungrouped: Vec<Group>,
    }

    impl Default for Inventory {
        fn default () -> Inventory {
            Inventory{
                name: 0.to_string(),
                hosts: Vec::new(),
                groups: Vec::new(),
                vars: Vec::new(),
                current_group: 0.to_string(),
                ungrouped: Vec::new(),
            }
        }
    }

    pub fn initialize() {
        println!("initializing...\n");
        //let ungrouped: Group,
        //Group::name = "ungrouped".fromString()
    }

}


// There are two default groups: all and ungrouped. all contains every host. 
// ungrouped contains all hosts that don’t have another group aside from all.

// /etc/ansible/group_vars/raleigh # can optionally end in '.yml', '.yaml', or '.json'
// /etc/ansible/group_vars/webservers
// /etc/ansible/host_vars/foosball

// The data in the groupfile ‘/etc/ansible/group_vars/raleigh’ for the ‘raleigh’ group might look like:

// ---
// ntp_server: acme.example.org
// database_server: storage.example.org



// [atlanta]
// host1 http_port=80 maxRequestsPerChild=808
// host2 http_port=303 maxRequestsPerChild=909

// [databases]
// db-[a:f].example.com

// HOST vars

// [atlanta]
// host1 http_port=80 maxRequestsPerChild=808
// host2 http_port=303 maxRequestsPerChild=909

// GROUP VARS

// [atlanta]
// host1
// host2

// [atlanta:vars]
// ntp_server=ntp.atlanta.example.com
// proxy=proxy.atlanta.example.com

// CHILDREN

// [atlanta]
// host1
// host2

// [raleigh]
// host2
// host3

// [southeast:children]
// atlanta
// raleigh