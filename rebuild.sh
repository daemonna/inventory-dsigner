#!/bin/bash

go build inventory_dsigner.go
go install

inventory_dsigner --inputfile="examples/inventory1.yml,examples/inventory2.yml,examples/inventory3.yml"  --outputfile="examples_output/generated.yml"
