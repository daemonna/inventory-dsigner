FROM fedora:latest as golang-f
RUN dnf install golang -y 

FROM golang-f as invdsigner
RUN go get codeberg.org/daemonna/inventory-dsigner
RUN go build codeberg.org/daemonna/inventory-dsigner
RUN go install codeberg.org/daemonna/inventory-dsigner
RUN inventory-dsigner
#RUN go install

#invcomb --inputfile="examples/inventory1.yml,examples/inventory2.yml,examples/inventory3.yml"  --outputfile="examples_output/generated.yml"
#COPY target/debug/inventory-dsigner /usr/bin/inventory-dsigner
#COPY inventory-dsigner /usr/bin/inventory-dsigner

# --disableplugin=subscription-manager golang cargo \\

  # && dnf --disableplugin=subscription-manager clean all

USER 1001

#ENTRYPOINT /usr/bin/inventory-dsigner
CMD inventory-dsigner