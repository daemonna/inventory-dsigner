package main

import (
	"flag"
	"fmt"
	"inventory-dsigner/controllers"
	"inventory-dsigner/models"
	"os"
)

func main() {
	fmt.Println("inventory combinator 0.0.2")

	inputfile := flag.String("inputfile", "", "inventory to read")
	inputfolder := flag.String("inputfolder", "", "folder to read")
	outputfile := flag.String("outputfile", "generated.yml", "outputfile")
	//outputfolder := flag.String("outputfolder", "", "folder to generate")
	flag.Parse()

	if *inputfile == "" {
		if *inputfolder == "" {
			fmt.Println("missing input file")
			os.Exit(1)
		}
	}

	models.InitInventory(*outputfile)
	controllers.ProcessInput(*inputfile)

	if *outputfile != "" {
		controllers.WriteSingleInventoryFile(*outputfile, true)
	}

}
